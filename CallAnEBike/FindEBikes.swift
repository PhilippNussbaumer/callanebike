//
//  FirstViewController.swift
//  CallAnEBike
//
//  Created by student on 4/4/16.
//  Copyright © 2016 student. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

class FindEBikes: UIViewController, CLLocationManagerDelegate
{
    @IBOutlet weak var googleMap: GMSMapView!
    
    let getPositionsURL = NSURL(string: "http://nussi.no-ip.info:1234/getLastPosition")
    
    let locationManager = CLLocationManager()
    var didFindMyLocation = false
    
    var eBikePositions = [EBikePosition]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        googleMap.addObserver(self, forKeyPath: "myLocation", options: NSKeyValueObservingOptions.New, context: nil)
        
        let camera: GMSCameraPosition = GMSCameraPosition.cameraWithLatitude(48.005, longitude: 13.61, zoom: 10.0)
        googleMap.camera = camera
        
        self.getEBikePositions()
    }

    override func viewDidDisappear(animated: Bool)
    {
        super.viewDidDisappear(animated)
        
        locationManager.stopUpdatingLocation()
    }
    
    @IBAction func navigateToEBike(sender: AnyObject)
    {
        if(UIApplication.sharedApplication().canOpenURL(NSURL(string:"comgooglemaps://")!))
        {
            UIApplication.sharedApplication().openURL(NSURL(string:
                "comgooglemaps://?saddr=&daddr=\(eBikePositions[0].latitude),\(eBikePositions[0].longitude)&directionsmode=walking")!)
            
        }
        else
        {
            print("Can't use Google Maps -> fallback to Apple Maps")
            UIApplication.sharedApplication().openURL(NSURL(string:
                "http://maps.apple.com/?daddr=\(eBikePositions[0].latitude),\(eBikePositions[0].longitude)&dirflg=w")!)
        }
    }
    
    func getEBikePositions()
    {
        let task = NSURLSession.sharedSession().dataTaskWithURL(getPositionsURL!)
        {
                (data, response, error) in
                if error != nil {
                    print("error fetching data")
                    return
                }
                
                var json: [String: AnyObject]
                do  {
                    json = try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableContainers) as! [String: AnyObject]
                } catch {
                    return
                }
                
                self.eBikePositions.append(EBikePosition(itemJSON: json))
            
                dispatch_async(dispatch_get_main_queue(),
                {
                    //show all found ebikes in map
                    for pos in self.eBikePositions
                    {
                        //calc standing time
                        let lockDate = NSDate(timeIntervalSince1970: Double(pos.time)/1000)
                        let timeDiff = NSDate().timeIntervalSinceDate(lockDate)
                        
                        let marker = GMSMarker()
                        marker.position = CLLocationCoordinate2DMake(pos.latitude, pos.longitude)
                        marker.title = "E-Bike #1"
                        marker.snippet = "Since: "+self.stringFromTimeInterval(timeDiff)
                        marker.appearAnimation = kGMSMarkerAnimationPop
                        marker.groundAnchor = CGPointMake(0.5, 0.5);
                        marker.icon = UIImage(data: UIImagePNGRepresentation(UIImage(named: "bike.png")!)!, scale: 3.0)
                        marker.map = self.googleMap
                        print("marker added")
                    }
                })
        }
        task.resume()
    }
    
    func stringFromTimeInterval(interval: Double) -> String
    {
        let ti = Int(interval)
        let days = (ti / 86400)
        let hours = (ti - days*86400) / 3600
        let minutes = (ti - days*86400 - hours*3600) / 60
        
        if(days != 0)
        {
            return String(format: "%d Days %d Hours %d Minutes",days,hours,minutes)
        }
        else if(hours != 0)
        {
            return String(format: "%d Hours %d Minutes",hours,minutes)
        }
        return String(format: "%d Minutes",minutes)
    }
    
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus)
    {
        if status == CLAuthorizationStatus.AuthorizedWhenInUse
        {
            googleMap.myLocationEnabled = true
        }
    }
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>)
    {
        if !didFindMyLocation
        {
            let myLocation: CLLocation = change![NSKeyValueChangeNewKey] as! CLLocation
            googleMap.camera = GMSCameraPosition.cameraWithTarget(myLocation.coordinate, zoom: 10.0)
            googleMap.settings.myLocationButton = true
            
            didFindMyLocation = true
        }
    }
}