//
//  SecondViewController.swift
//  CallAnEBike
//
//  Created by student on 4/4/16.
//  Copyright © 2016 student. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

class RideMode: UIViewController, CLLocationManagerDelegate
{
    @IBOutlet weak var googleMap: GMSMapView!
    @IBOutlet weak var curSpeed: UILabel!
    @IBOutlet weak var avgSpeed: UILabel!
    @IBOutlet weak var trip: UILabel!
    
    let locationManager = CLLocationManager()
    var didFindMyLocation = false
    
    var oldLocation: CLLocation!
    var firstTimestamp: NSDate!
    var timeBetween: NSTimeInterval!
    var distance = 0.0
    var avg = 0.0
    
    var route = GMSMutablePath()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        oldLocation = nil
        firstTimestamp = nil
        timeBetween = nil
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        googleMap.myLocationEnabled = true
        
        googleMap.addObserver(self, forKeyPath: "myLocation", options: NSKeyValueObservingOptions.New, context: nil)
        
        let camera: GMSCameraPosition = GMSCameraPosition.cameraWithLatitude(48.005, longitude: 13.61, zoom: 10.0)
        googleMap.camera = camera
    }
    
    override func viewDidDisappear(animated: Bool)
    {
        super.viewDidDisappear(animated)
        
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus)
    {
        if status == CLAuthorizationStatus.AuthorizedWhenInUse
        {
            googleMap.myLocationEnabled = true
        }
    }
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>)
    {
        let myLocation: CLLocation = change![NSKeyValueChangeNewKey] as! CLLocation
        route.addCoordinate(myLocation.coordinate)
        let path = GMSPolyline(path: route)
        path.strokeWidth = 4.0
        path.map = googleMap
        googleMap.camera = GMSCameraPosition.cameraWithTarget(myLocation.coordinate, zoom: googleMap.camera.zoom)
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let latestLocation: CLLocation = locations.first!
        
        let kmh = (locationManager.location?.speed)! * 3.6
        let kmhString = NSString(format: "%.1f", kmh)
        self.curSpeed.text = "Current Speed: \(kmhString) km/h"
        
        if oldLocation == nil
        {
            oldLocation = latestLocation
            firstTimestamp = latestLocation.timestamp
        }
        else
        {
            distance += latestLocation.distanceFromLocation(oldLocation) / 1000
            timeBetween = latestLocation.timestamp.timeIntervalSinceDate(firstTimestamp)
            let hours = Double(timeBetween)/3600
            avg = distance / hours
            oldLocation = latestLocation
        }
        let tripString = NSString(format: "%.2f", distance)
        self.trip.text = "Trip: \(tripString) km"
        
        let avgString = NSString(format: "%.1f", avg)
        self.avgSpeed.text = "Average Speed: \(avgString) km/h"
    }
}