//
//  EBikePosition.swift
//  CallAnEBike
//
//  Created by student on 4/4/16.
//  Copyright © 2016 student. All rights reserved.
//

import Foundation

class EBikePosition
{
    var time = 0
    var latitude = 0.0
    var longitude = 0.0
    
    init(itemJSON: [String: AnyObject])
    {
        guard let time = itemJSON["time"] as? Int
            else {
                return
        }
        self.time = time
        
        guard let latitude = itemJSON["latitude"] as? Double
            else {
                return
        }
        self.latitude = latitude
        
        guard let longitude = itemJSON["longitude"] as? Double
            else {
                return
        }
        self.longitude = longitude
        print(time,latitude,longitude)
    }
}
